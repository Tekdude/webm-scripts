@set /P height=Enter height: 
@set /P start=Enter start time: 
@set /P end=Enter end time: 
@set /P bitrate=Enter bitrate (M): 
:start
%~dp0ffmpeg.exe -i %1 -ss %start% -to %end% -c:v libvpx-vp9 -b:v %bitrate%M -maxrate %bitrate%M -filter:v scale=-1:%height% -an -pass 1 -passlogfile %TEMP%\ -map_metadata -1 -y "%~dp1output.webm" && ^
%~dp0ffmpeg.exe -i %1 -ss %start% -to %end% -c:v libvpx-vp9 -b:v %bitrate%M -maxrate %bitrate%M -filter:v scale=-1:%height% -an -pass 2 -passlogfile %TEMP%\ -map_metadata -1 -y "%~dp1output.webm"
@set /P overwrite=Overwrite original (Y/N): 
@if %overwrite%==Y goto overwrite
@if %overwrite%==y goto overwrite
@set /P height=Enter height (%height%): 
@set /P start=Enter start time (%start%): 
@set /P end=Enter end time (%end%): 
@set /P bitrate=Enter bitrate (M) (%bitrate%): 
@goto start

:overwrite
@del %1
@move /Y "%~dp1output.webm" "%~dpn1.webm"
