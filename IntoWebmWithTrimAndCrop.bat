@set /P x=Enter X: 
@set /P y=Enter Y: 
@set /P cwidth=Enter crop width: 
@set /P cheight=Enter crop height: 
@set /P height=Enter output height: 
@set /P start=Enter start time: 
@set /P end=Enter end time: 
@set /P bitrate=Enter bitrate (M): 
:start
%~dp0ffmpeg.exe -i %1 -ss %start% -to %end% -c:v libvpx-vp9 -b:v %bitrate%M -maxrate %bitrate%M -vf crop=%cwidth%:%cheight%:%x%:%y%,scale=-1:%height% -an -pass 1 -passlogfile %TEMP%\ -map_metadata -1 -y "%~dp1output.webm" && ^
%~dp0ffmpeg.exe -i %1 -ss %start% -to %end% -c:v libvpx-vp9 -b:v %bitrate%M -maxrate %bitrate%M -vf crop=%cwidth%:%cheight%:%x%:%y%,scale=-1:%height% -an -pass 2 -passlogfile %TEMP%\ -map_metadata -1 -y "%~dp1output.webm"
@set /P overwrite=Overwrite original (Y/N): 
@if %overwrite%==Y goto overwrite
@if %overwrite%==y goto overwrite
@set /P x=Enter X (%x%): 
@set /P y=Enter Y (%y%): 
@set /P cwidth=Enter crop width (%cwidth%): 
@set /P cheight=Enter crop height (%cheight%): 
@set /P height=Enter output height (%height%): 
@set /P start=Enter start time (%start%): 
@set /P end=Enter end time (%end%): 
@set /P bitrate=Enter bitrate (M) (%bitrate%): 
@goto start

:overwrite
@del %1
@move /Y "%~dp1output.webm" "%~dpn1.webm"
