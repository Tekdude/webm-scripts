# WEBM Scripts

Some useful Windows Batch scripts for converting videos into WEBM files.

## Usage

1. To "install" the scripts, copy the `.bat` files into your FFmpeg `\bin` folder.
2. To convert a video, select the file, drag it on top of the script you want to run, then release.
3. A console will open and ask for conversion parameters (if any).
4. FFmpeg will convert the video, and write the result to `output.webm` in the same locaion as the source video.
5. The console will ask for the conversion parameters again, and show the previous values, in case you want to convert the video again.
6. Close the console when you are done.
